from distutils.core import setup

setup(
    name='PySimpleNetwork',
    version='0.1',
    packages=['pysimplenetwork',],
    license='Create Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    author='Alexandre Plateau',
    author_email='lexplt.dev@gmail.com',
    url='https://gitlab.com/SuperFola/PySimpleNetwork.git'
)
