PySimpleNetwork
===============

A small Python >= 3.6 library to handle easily UDP and RUDP sockets.

The library takes care of the size of the messages sent by itself.

Installation
============

~$ git clone https://gitlab.com/SuperFola/PySimpleNetwork.git
~$ cd PySimpleNetwork/
~/PySimpleNetwork/$ python3 setup.py sdist
~/PySimpleNetwork/$ pip3 install pysimplenetwork-[version].tar.gz

Running the tests
=================

~$ python3.6 -m pysimplenetwork

Using
=====

>>> import pysimplenetwork
>>> client = pysimplenetwork.SimpleUDPSock(host='127.0.0.1', port=52000)
>>> server = pysimplenetwork.SimpleUDPSock(host='127.0.0.1', port=52000, bind=True)
>>> client.send("hello world".encode('utf-8'))
>>> print(server.recv().decode('utf-8'))
