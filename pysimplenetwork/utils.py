# coding: utf-8

def to_bytes(n: int) -> bytes:
    output = []
    while True:
        output.append(n & 0xff)
        if n > 0xff:
            n >>= 8
        else:
            break
    return bytes(output[::-1])


def to_int(b: bytes) -> int:
    output = 0
    for i, e in enumerate(b[::-1]):
        output += e << (i * 8)
    return output
