import socket
from .simple_sock import SimpleSock


class SimpleUDPSock(SimpleSock):
    def __init__(self, host=None, port=8080, bind=False):
        super().__init__(host, port, bind, (socket.AF_INET, socket.SOCK_DGRAM))

    def _send(self, data, destination):
        self.sock.sendto(data, destination)

    def _recv(self, bufsize):
        return self.sock.recvfrom(bufsize)
