# coding: utf-8

import socket
import struct


MAX_PACK_SIZE = 2**15 - 1


class SimpleSockConfigurationError(Exception):
    pass


class Pack:
    def __init__(self, data):
        self.size = len(data)
        self.data = data

    def pack(self):
        return struct.pack(f"I{self.size}s", self.size, self.data)

    @staticmethod
    def unpack(data):
        size, *_ = struct.unpack("I", data[:4])
        content, *_ = struct.unpack(f"{size}s", data[4:])
        return size, content


class SimpleSock:
    def __init__(self, host=None, port=8080, bind=False, sock_data=None):
        self.host = host or socket.gethostbyname(socket.gethostname())
        self.port = port

        if not sock_data:
            raise SimpleSockConfigurationError

        self.sock = socket.socket(*sock_data)

        if bind:
            self.sock.bind((self.host, self.port))

    def _send(self, data, destination):
        raise NotImplementedError

    def _recv(self, bufsize):
        raise NotImplementedError

    def send(self, msg):
        size = len(msg)

        if size < MAX_PACK_SIZE:
            pack = Pack(msg)
            self._send(pack.pack(), (self.host, self.port))
        else:
            while size:
                if size >= MAX_PACK_SIZE:
                    pack = Pack(msg[:MAX_PACK_SIZE - 4])
                    self._send(pack.pack(), (self.host, self.port))
                    msg = msg[MAX_PACK_SIZE - 4:]
                else:
                    pack = Pack(msg)
                    self._send(pack.pack(), (self.host, self.port))
                    msg = msg[size:]
                size = len(msg)

    def recv(self):
        msg = b""

        while True:
            data, _ = self._recv(MAX_PACK_SIZE)
            size, content = Pack.unpack(data)
            msg += content

            if size < MAX_PACK_SIZE - 4:
                break

        return msg

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def __del__(self):
        self.close()

    def close(self):
        self.sock.close()
